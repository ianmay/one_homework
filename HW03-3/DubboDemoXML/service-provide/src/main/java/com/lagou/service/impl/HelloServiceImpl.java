package com.lagou.service.impl;

import com.lagou.service.HelloService;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "hello" + name;
    }

    @Override
    public String methodA() {
        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "This is A";
    }

    @Override
    public String methodB() {
        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "This is B";
    }

    @Override
    public String methodC() {
        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "This is C";
    }
}
