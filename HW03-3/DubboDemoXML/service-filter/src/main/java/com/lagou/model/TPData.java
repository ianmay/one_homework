package com.lagou.model;

public class TPData {
    //入队时间
    public long timeStamp;
    //接口调用耗时
    public long duration;

    public TPData() {
    }

    public TPData(long timeStamp, long duration) {
        this.timeStamp = timeStamp;
        this.duration = duration;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
